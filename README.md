Какой документации не хватает, на мой взгляд:
- Общего описания того, какие фичи/виджеты/сервисы находятся в зоне нашей ответсвенности. 
- Описания параметров и хэндлеров в API и переиспользуемых компонентах. В layout'е много пересекающихся доменов, по названиям не всегда очевидно, для чего существует тот или иной параметр и откуда он берется. 
- Какие доступы нужны для работы с бэкендом ( ͡° ͜ʖ ͡°) 

С учетом того, что мы вводим практику фича-лидства, возможно стоит попытаться престроить процесс работы над задачей. Как вариант:
1. Фича-лид собирает и формализует требования вместе с продактом. На выходе должны получить **примерное ТЗ**.
2. Фича-лид верхнеуровнево прорабатывает фичу, привлекая по необходимости других участников команды. Скорее всего, на данном этапе **ТЗ** дополняется новыми требованиями, которые не были учтены. На выходе получаем **техническое описание реализации** (а-ля mini-TDR). 
3. Нарезаются эпики, проводятся необходимые грумминги в зависимости от комплексности задачи. 
4. По мере работы над задачами техническое описание дополняется актуальными изменениями в реализации. На выходе получаем **техническую документацию**. 

Возможные пункты технического описания реализации:
- Введение нового сервиса - обоснование, API, схема взаимодействия.  
- Обновление API - добавление/изменение хэндлеров, параметров. 
- Новые/обновленные интеграции. В случае сложного взаимодействия можно накидать схемку. 
- Новые/обновленные таблицы в БД. 
- Описание AB/версионирование/аналитика. 